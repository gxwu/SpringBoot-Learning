package com.didispace.chapter310;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class Chapter310ApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private BizService bizService;

    @Autowired
    private ClassA a;


    @Test
    public void test00(){
        System.out.println("tes a "+a);
    }

    @Test
    @Transactional
    public void test1() throws Exception {
        // 创建10条记录
        userRepository.save(new User("AAA", 10));
        userRepository.save(new User("BBB", 20));
        userRepository.save(new User("CCC", 30));
        userRepository.save(new User("DDD", 40));
        userRepository.save(new User("EEE", 50));
        userRepository.save(new User("FFF", 60));
        userRepository.save(new User("GGG", 70));
        userRepository.save(new User("HHH", 80));
        userRepository.save(new User("III", 90));
        userRepository.save(new User("JJJ", 100));

        // 测试findAll, 查询所有记录
        Assert.assertEquals(10, userRepository.findAll().size());

        // 测试findByName, 查询姓名为FFF的User
        Assert.assertEquals(60, userRepository.findByName("FFF").getAge().longValue());

        // 测试findUser, 查询姓名为FFF的User
        Assert.assertEquals(60, userRepository.findUser("FFF").getAge().longValue());

        // 测试findByNameAndAge, 查询姓名为FFF并且年龄为60的User
        Assert.assertEquals("FFF", userRepository.findByNameAndAge("FFF", 60).getName());

        // 测试删除姓名为AAA的User
        userRepository.delete(userRepository.findByName("AAA"));

        // 测试findAll, 查询所有记录, 验证上面的删除是否成功
        Assert.assertEquals(9, userRepository.findAll().size());

    }

    /**
     * 异常抛出 与 回滚
     */
    @Test
    public void test01() {
        //1 正常插入
        userService.insert01(new User("A", 11));
        //2 运行时异常，数据回滚
        try {
            userService.insert02RuntimeException(new User("B", 22));
        }catch (Exception e) {
            e.printStackTrace();
            log.error("12：", e.getMessage());
        }
        //3 非运行时异常，无法回滚
        try {
            userService.insert03Exception(new User("C", 33));
        }catch (Exception e) {
            log.error("13：", e.getMessage());
        }
        //4 内部捕获异常，正常插入
        userService.insert04CatchRuntimeException(new User("D", 44));

    }

    /**
     * 同一个类内事务方法嵌套调用其他方法
     * 非事务方法调事务方法时实际走的不是动态代理类，而是被代理类包裹的实际实现类中自己的方法，所以不会被 Spring AOP 切到，也就导致 @Transactional 事务注解不生效
     */
    @Test
    public void test02() {
        //1、普通方法，内嵌事务方法，事务异常无法回滚
        try{
            userService.insert05SameClassInnerInvoke(new User("EE", 11, "同一类中事务调用"));
        }catch (RuntimeException e) {
            log.error("11：", e.getMessage());

        }
        //2、事务方法，内嵌普通方法，事务异常可以回滚
        try {
            userService.insert06SameClassNestNormal(new User("FF", 21, "同一类中事务调用"));
        }catch (RuntimeException e) {
            log.error("21：", e.getMessage());
        }
        //3、事务方法，内嵌调用同一类其他事务方法，其他事务方法只能以普通方法的形式进行运行
        try {
            userService.insertO7SameClassNestTransaction(new User("GG", 31, "同一类中事务调用"));
        }catch (RuntimeException e) {
            log.error("31：", e.getMessage());
        }
        //4、事务方法，内嵌调用同一类其他事务方法，其他事务方法只能以普通方法的形式进行运行，外层捕获子事务异常，相当于捕获普通方法异常
        try {
            userService.insertO8SameClassNestTransactionCatch(new User("HH", 41, "同一类中事务调用"));
        }catch (RuntimeException e) {
            log.error("41：", e.getMessage());
        }

        //5、子事务抛出异常，无论外层是否俘获子事务，父子事务都会全部回滚
        try {
            bizService.insertSameClassCatch(new User("II", 51, "外层类调用同一类中事务调用"));
        }catch (RuntimeException e) {
            log.error("51：", e.getMessage());
        }
    }

    /**
     * 事务传递测试
     * PROPAGATION_REQUIRED（默认）
     * 表示当前方法必须运行在事务中。如果当前事务存在，方法将会在该事务中运行。否则，会启动一个新的事务。
     */
    @Test
    public void test03Required(){
        //0、正常处理，不抛出异常
        bizService.insertRequiredSuccess(new User("FF", 0, "required"));

        //1、子事务抛出异常，父子事务全部回滚
        try{
            bizService.insertRequired(new User("HH", 11, "required"));
        }catch (RuntimeException e) {
            log.error("11：", e.getMessage());

        }

        //2、子事务抛出异常，无论外层是否捕获子事务，父子事务都会全部回滚
        try {
            bizService.insertRequiredCatch(new User("GG", 21, "required catch"));
        }catch (RuntimeException e) {
            log.error("21：", e.getMessage());
        }
    }

    @Test
    public void test04Support(){
        //1、外层没有事务，内层support相当于无效
        try{
            bizService.insertSupportNotTransaction(new User("HH", 11, "support"));
        }catch (RuntimeException e) {
            log.error("11：", e.getMessage());
        }
        //2、外层有事务，内层support相当于required
        try{
            bizService.insertSupport(new User("II", 21, "support"));
        }catch (RuntimeException e) {
            log.error("21：", e.getMessage());
        }
    }

    @Test
    public void test04Mandatory(){
        //1、外层没有事务，抛出异常
        try{
            bizService.insertMandatoryNotTransaction(new User("HH", 11, "Mandatory"));
        }catch (RuntimeException e) {
            log.error("11：", e.getMessage());
            e.printStackTrace();
        }
        //2、外层有事务，内层Mandatory相当于required
        try{
            bizService.insertMandatory(new User("II", 21, "Mandatory"));
        }catch (RuntimeException e) {
            log.error("21：", e.getMessage());
        }
    }

    @Test
    public void test05RequiresNew(){
        //1、内层以新事务方式启动，不受外层影响
        try{
            bizService.insertRequiresNew(new User("HH", 11, "requires_new"));
        }catch (RuntimeException e) {
            log.error("11：", e.getMessage());
        }
        //2、内层回滚，导致外层回滚
        try{
            bizService.insertRequiresNewRuntimeException(new User("II", 21, "requires_new"));
        }catch (RuntimeException e) {
            log.error("21：", e.getMessage());
        }
        //3、内层回滚，外层捕获异常，阻止外层回滚
        try{
            bizService.insertRequiresNewRuntimeExceptionCatch(new User("JJ", 31, "requires_new"));
        }catch (RuntimeException e) {
            log.error("31：", e.getMessage());
        }
    }

    @Test
    public void test05NotSupport(){
        //1、表示该方法不应该运行在事务中。如果存在当前事务，在该方法运行期间，当前事务将被挂起
        try{
            bizService.insertNotSupported(new User("HH", 11, "not supported"));
        }catch (RuntimeException e) {
            log.error("11：", e.getMessage());
        }
    }
    @Test
    public void test05Never(){
        //1、表示当前方法不应该运行在事务上下文中。如果当前正有一个事务在运行，则会抛出异常
        try{
            bizService.insertNever(new User("HH", 11, "never"));
        }catch (RuntimeException e) {
            log.error("11：", e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 表示如果当前已经存在一个事务，那么该方法将会在嵌套事务中运行。嵌套的事务可以独立于当前事务进行单独地
     * 提交或回滚。如果当前事务不存在，那么其行为与PROPAGATION_REQUIRED一样。
     */
    @Test
    public void test06Nested(){
        //1、
        try{
            bizService.insertNestedNotTransaction(new User("HH", 11, "nested"));
        }catch (RuntimeException e) {
            log.error("11：", e.getMessage());
            e.printStackTrace();
        }
        //2、
        try{
            bizService.insertNestedParentException(new User("II", 21, "nested"));
        }catch (RuntimeException e) {
            log.error("21：", e.getMessage());
            e.printStackTrace();
        }
        //3、
        try{
            bizService.insertNestedParentException(new User("JJ", 31, "nested"));
        }catch (RuntimeException e) {
            log.error("31：", e.getMessage());
            e.printStackTrace();
        }
    }


}
