package com.didispace.chapter310;

import lombok.Data;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author: gx.wu
 * @create: 2023-03-28 19:29
 * @Description:
 */
@Data
@Component
public class ClassA implements BeanFactoryAware {

    private String msg = "class a";
    public static AtomicInteger v = new AtomicInteger();

//    @Autowired
    private ClassB b;

//    @Autowired
//    public ClassA(ClassB b) {
//        this.b = b;
//    }

    @Autowired
    public void setClassB(ClassB b) {
        System.out.println("class a set function");
        this.b = b;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("class a "+ v.getAndIncrement());
    }
}
