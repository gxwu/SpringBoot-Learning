package com.didispace.chapter310;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author: gx.wu
 * @create: 2023-03-20 09:50
 * @Description:
 */
public interface GoodsRepository extends JpaRepository<Goods,Long> {

}
