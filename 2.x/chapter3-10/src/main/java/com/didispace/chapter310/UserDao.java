package com.didispace.chapter310;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author: gx.wu
 * @create: 2023-03-20 16:25
 * @Description:
 */
@Repository
public class UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int save(User user){
        String sql = "INSERT INTO user (id, name, age, remark) VALUES(?,?,?,?)";
        return jdbcTemplate.update(sql, user.getAge(), user.getName(), user.getAge(), user.getRemark());
    }

}
