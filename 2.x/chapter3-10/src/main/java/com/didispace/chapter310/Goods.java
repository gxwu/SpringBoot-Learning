package com.didispace.chapter310;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author: gx.wu
 * @create: 2023-03-20 09:44
 * @Description:
 */
@Entity
@Data
@NoArgsConstructor
public class Goods {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private String name;

    private Long price;

    private String remark;

    public Goods(String name, Long price, String remark){
        this.name = name;
        this.price = price;
        this.remark = remark;
    }
}
