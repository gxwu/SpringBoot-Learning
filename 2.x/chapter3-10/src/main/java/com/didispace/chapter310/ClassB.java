package com.didispace.chapter310;

import lombok.Data;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author: gx.wu
 * @create: 2023-03-28 19:29
 * @Description:
 */
@Data
@Component
public class ClassB implements BeanFactoryAware {

    private String msg = "class b";
    public static AtomicInteger v = new AtomicInteger();

//    @Autowired
    private ClassC c;

//    @Autowired
//    public ClassB(ClassC c) {
//        this.c = c;
//    }

    @Autowired
    public void setClassC(ClassC c) {
        System.out.println("class b set function");
        this.c = c;
    }


    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("class b "+ v.getAndIncrement());
    }
}
