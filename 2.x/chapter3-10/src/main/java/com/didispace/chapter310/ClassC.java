package com.didispace.chapter310;

import lombok.Data;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author: gx.wu
 * @create: 2023-03-28 19:30
 * @Description:
 */
@Data
@Component
public class ClassC implements BeanFactoryAware {

    private String msg = "class c";
    public static AtomicInteger v = new AtomicInteger();

//    @Autowired
    private ClassA a;

//    @Autowired
//    public ClassC(ClassA a) {
//        this.a = a;
//    }

    @Autowired
    public void setClassA(ClassA a){
        System.out.println("class c set function");
        this.a = a;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("class c "+ v.getAndIncrement());
    }
}
