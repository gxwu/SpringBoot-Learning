package com.didispace.chapter310;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: gx.wu
 * @create: 2023-03-20 09:56
 * @Description:
 */
@Slf4j
@Service
public class BizService {

    @Autowired
    private UserService userService;

    @Autowired
    private GoodsService goodsService;

    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertSameClassCatch(User user) {
        userService.insert01(user);
        try {
            userService.insert02RuntimeException(new User(user.getName(), user.getAge()+1, user.getRemark()));
        }catch (RuntimeException e) {
            log.error("异常：", e.getMessage());
        }
    }

    public void insertRequiredSuccess(User user) {
        userService.insert01(user);
        goodsService.insert01(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }
    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertRequired(User user) {
        userService.insert01(user);
        goodsService.insert02RuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertRequiredCatch(User user) {
        userService.insert01(user);
        try {
            goodsService.insert02RuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
        }catch (RuntimeException e) {
            log.error("异常：", e.getMessage());
        }
    }

    public void insertSupportNotTransaction(User user) {
        userService.insert00Normal(user);
        goodsService.insert03SupportRuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertSupport(User user) {
        userService.insert00Normal(user);
        goodsService.insert03SupportRuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    public void insertMandatoryNotTransaction(User user) {
        userService.insert00Normal(user);
        goodsService.insert04MandatoryRuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertMandatory(User user) {
        userService.insert00Normal(user);
        goodsService.insert04MandatoryRuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertRequiresNew(User user) {
        userService.insert09RequiresNewNormal(user);
        goodsService.insert02RuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertRequiresNewRuntimeException(User user) {
        userService.insert00Normal(user);
        goodsService.insert07RequiresNewRuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }


    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertRequiresNewRuntimeExceptionCatch(User user) {
        userService.insert00Normal(user);
        try {
            goodsService.insert07RequiresNewRuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
        }catch (RuntimeException e){
            log.error("异常", e);
        }
    }


    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertNotSupported(User user) {
        userService.insert10NotSupportedNormal(user);
        goodsService.insert02RuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertNever(User user) {
        userService.insert11NeverNormal(user);
        goodsService.insert02RuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    public void insertNestedNotTransaction(User user) {
        goodsService.insert06NestedRuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertNestedParentException(User user) {
        userService.insert12NestedNormal(user);
        goodsService.insert02RuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }


    @Transactional(propagation =  Propagation.REQUIRED)
    public void insertNestedChildException(User user) {
        userService.insert00Normal(user);
        goodsService.insert06NestedRuntimeException(new Goods(user.getName(), (long) (user.getAge()+1), user.getRemark()));
    }




}
