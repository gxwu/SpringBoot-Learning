package com.didispace.chapter310;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author: gx.wu
 * @create: 2023-03-20 16:25
 * @Description:
 */
@Repository
public class GoodsDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int save(Goods goods){
        String sql = "INSERT INTO goods (id, name, price, remark) VALUES(?,?,?,?)";
        return jdbcTemplate.update(sql, goods.getPrice(), goods.getName(), goods.getPrice(), goods.getRemark());
    }

}
