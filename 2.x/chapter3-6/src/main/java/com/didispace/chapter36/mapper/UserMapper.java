package com.didispace.chapter36.mapper;

import com.didispace.chapter36.entity.User;
import com.didispace.chapter36.entity.User2;
import com.didispace.chapter36.entity.User3;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 程序猿DD/翟永超 on 2020/2/28.
 * <p>
 * Blog: http://blog.didispace.com/
 * Github: https://github.com/dyc87112/
 */
public interface UserMapper {

    User findByName(@Param("name") String name);

    int insert(@Param("name") String name, @Param("age") Integer age);

    int insertReturnId(User user);

    int insertReturnId2(User user);

    int batchInsert(List<User> userList);

    List<User2> findAllUser2();

    List<User2> findAllUser3();

    List<User3> findAllUser4();

    List<User3> findAllUser5();
}
