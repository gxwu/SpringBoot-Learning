package com.didispace.chapter36.config;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: gx.wu
 * @create: 2023-04-24 00:11
 * @Description:
 */

@Configuration
public class TopicRabbitConfig {

    /** 绑定队列 */
    public final static String QUEUE_MALE = "topic-queue-male";
    public final static String QUEUE_FEMALE = "topic-queue-female";
    public final static String QUEUE_HUMAN = "topic-queue-human";
    /** 绑定路由key */
    public final static String ROUTER_KEY_ALL_MALE = "people.male.#";
    public final static String ROUTER_KEY_ALL_FEMALE = "people.female.#";
    public final static String ROUTER_KEY_ALL_HUMAN = "people.human.#";
    public final static String ROUTER_KEY_ALL_BABY = "people.*.baby";
    public final static String ROUTER_KEY_ALL_ADULT = "people.*.adult";
    /** 交换机 */
    public final static String PEOPLE_EXCHANGE = "topicPeopleExchange";

    @Bean
    public Queue QueueMale() {
        return new Queue(QUEUE_MALE);
    }

    @Bean
    public Queue QueueFemale() {
        return new Queue(QUEUE_FEMALE);
    }

    @Bean
    public Queue QueueHuman() {
        return new Queue(QUEUE_HUMAN);
    }

    @Bean
    TopicExchange peopleExchange() {
        return new TopicExchange(PEOPLE_EXCHANGE);
    }

    @Bean
    Binding bindAllMan(){
        return BindingBuilder.bind( QueueMale() ).to( peopleExchange() ).with(ROUTER_KEY_ALL_MALE);
    }
    @Bean
    Binding bindAllFemale(){
        return BindingBuilder.bind( QueueFemale() ).to( peopleExchange() ).with(ROUTER_KEY_ALL_FEMALE);
    }
    @Bean
    Binding bindAllHuman(){
        return BindingBuilder.bind( QueueHuman() ).to( peopleExchange() ).with(ROUTER_KEY_ALL_HUMAN);
    }
    @Bean
    Binding bindAllBaby(){
        return BindingBuilder.bind( QueueHuman() ).to( peopleExchange() ).with(ROUTER_KEY_ALL_BABY);
    }
    @Bean
    Binding bindAllAdult(){
        return BindingBuilder.bind( QueueHuman() ).to( peopleExchange() ).with(ROUTER_KEY_ALL_ADULT);
    }




}
