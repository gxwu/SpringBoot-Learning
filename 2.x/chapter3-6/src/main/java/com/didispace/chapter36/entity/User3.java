package com.didispace.chapter36.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
public class User3 {
    private Long id;
    private String name;
    private Integer age;
    private List<Hobby> hobbys;

    public User3(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

}