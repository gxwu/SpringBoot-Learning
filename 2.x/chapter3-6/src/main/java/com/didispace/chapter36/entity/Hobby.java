package com.didispace.chapter36.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author: gx.wu
 * @create: 2023-04-21 23:46
 * @Description:
 */
@Data
@NoArgsConstructor
public class Hobby {
    private Long id;
    private Long userid;
    private String hobby;
    private String remark;

}
