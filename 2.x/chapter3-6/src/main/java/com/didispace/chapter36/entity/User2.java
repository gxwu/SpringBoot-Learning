package com.didispace.chapter36.entity;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class User2 {
    private Long id;
    private String name;
    private Integer age;
    private Hobby hobby;

    public User2(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

}