package com.didispace.chapter36.controller;
import com.didispace.chapter36.config.TopicRabbitConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
/**
 * @author: gx.wu
 * @create: 2023-04-23 23:06
 * @Description:
 */

@RestController
public class SendMessageController {

    //使用RabbitTemplate,这提供了接收/发送等等方法
    @Autowired
    RabbitTemplate rabbitTemplate;

    @GetMapping("/sendDirectMessage")
    public String sendDirectMessage() {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "test message, hello!";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String,Object> map=new HashMap<>();
        map.put("messageId",messageId);
        map.put("messageData",messageData);
        map.put("createTime",createTime);
        //将消息携带绑定键值：TestDirectRouting 发送到交换机TestDirectExchange
        rabbitTemplate.convertAndSend("TestDirectExchange", "TestDirectRouting", map);
        return "ok";
    }

    @GetMapping("/sendDemo")
    public String sendDemo() {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "demo message, hello!";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String,Object> map=new HashMap<>();
        map.put("messageId",messageId);
        map.put("messageData",messageData);
        map.put("createTime",createTime);
        rabbitTemplate.convertAndSend("DemoQueue", map);
        return "ok";
    }


    @GetMapping("/sendTopicMessage1")
    public String sendTopicMessage1(@RequestParam(name = "testCase", defaultValue = "1") int testCase) {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "message: man-baby ";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String, Object> manMap = new HashMap<>();
        manMap.put("messageId", messageId);
        manMap.put("messageData", messageData);
        manMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend(TopicRabbitConfig.PEOPLE_EXCHANGE, "people.male.baby", manMap);
        return "ok";
    }

    @GetMapping("/sendTopicMessage2")
    public String sendTopicMessage2(@RequestParam(name = "testCase", defaultValue = "1") int testCase) {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "message: female-adult ";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String, Object> manMap = new HashMap<>();
        manMap.put("messageId", messageId);
        manMap.put("messageData", messageData);
        manMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend(TopicRabbitConfig.PEOPLE_EXCHANGE, "people.female.adult", manMap);
        return "ok";
    }

    @GetMapping("/sendTopicMessage3")
    public String sendTopicMessage3(@RequestParam(name = "testCase", defaultValue = "1") int testCase) {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "message: human-baby ";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String, Object> manMap = new HashMap<>();
        manMap.put("messageId", messageId);
        manMap.put("messageData", messageData);
        manMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend(TopicRabbitConfig.PEOPLE_EXCHANGE, "people.human.baby", manMap);
        return "ok";
    }

    @GetMapping("/sendTopicMessage4")
    public String sendTopicMessage4(@RequestParam(name = "testCase", defaultValue = "1") int testCase) {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "message: human-unknow ";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String, Object> manMap = new HashMap<>();
        manMap.put("messageId", messageId);
        manMap.put("messageData", messageData);
        manMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend(TopicRabbitConfig.PEOPLE_EXCHANGE, "people.human.", manMap);
        return "ok";
    }


    @GetMapping("/sendTopicMessageCustomizer")
    public String sendTopicMessageCustomizer(@RequestParam(name = "testCase", defaultValue = "1") int testCase, String routerKey) {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "message: customizer，routerKey：" + routerKey;
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String, Object> manMap = new HashMap<>();
        manMap.put("messageId", messageId);
        manMap.put("messageData", messageData);
        manMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend(TopicRabbitConfig.PEOPLE_EXCHANGE, routerKey, manMap);
        return "ok";
    }


    @GetMapping("/sendFanoutMessage")
    public String sendFanoutMessage() {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "message: testFanoutMessage ";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String, Object> map = new HashMap<>();
        map.put("messageId", messageId);
        map.put("messageData", messageData);
        map.put("createTime", createTime);
        rabbitTemplate.convertAndSend("fanoutExchange", null, map);
        return "ok";
    }
}
