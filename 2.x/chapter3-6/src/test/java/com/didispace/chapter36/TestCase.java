package com.didispace.chapter36;

import com.alibaba.fastjson.JSON;
import com.didispace.chapter36.entity.User;
import com.didispace.chapter36.entity.User2;
import com.didispace.chapter36.entity.User3;
import com.didispace.chapter36.mapper.UserMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: gx.wu
 * @create: 2023-04-21 21:31
 * @Description:
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestCase {

    @Autowired
    private UserMapper userMapper;


    @Test
    public void test01(){
        User u = new User("小李", 23);
        int r = userMapper.insertReturnId(u);
        System.out.println(u);
        System.out.println(r);
    }


    @Test
    public void test02(){
        //插入返回自增id
        User u = new User("小李2", 34);
        int r = userMapper.insertReturnId2(u);
        System.out.println(u);
        System.out.println(r);
    }

    @Test
    public void test03(){
        //批量插入，返回自增id
        List<User> userList = new ArrayList<>();
        userList.add(new User("小李", 23));
        userList.add(new User("小敏", 27));
        userList.add(new User("小红", 24));
        int r = userMapper.batchInsert(userList);
        System.out.println(userList);
        System.out.println(r);
    }

    @Test
    public void test04(){
        //一对一，联合查询
        List<User2> list = userMapper.findAllUser2();
        for(User2 u : list){
            System.out.println(JSON.toJSONString(u));
        }
    }

    @Test
    public void test05(){
        //一对一，嵌套查询
        List<User2> list = userMapper.findAllUser3();
        for(User2 u : list){
            System.out.println(JSON.toJSONString(u));
        }
    }

    @Test
    public void test06(){
        //一对多，联合查询
        List<User3> list = userMapper.findAllUser4();
        for(User3 u : list){
            System.out.println(JSON.toJSONString(u));
        }
    }

    @Test
    public void test07(){
        //一对多，嵌套查询
        List<User3> list = userMapper.findAllUser5();
        for(User3 u : list){
            System.out.println(JSON.toJSONString(u));
        }
    }

//    @Test
//    public void t(){
//        A a = new A(11, "ddd");
//        System.out.println(JSON.toJSONString(a));
//    }
//
//    @Data
//    @NoArgsConstructor
//    public static class A {
//        private int id;
//        private String name;
//
//        public A(int id, String name) {
//            this.id = id;
//            this.name = name;
//        }
//    }
}
