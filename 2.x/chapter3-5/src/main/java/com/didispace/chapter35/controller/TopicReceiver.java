package com.didispace.chapter35.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: gx.wu
 * @create: 2023-04-23 23:35
 * @Description:
 */

@Slf4j
@Component
public class TopicReceiver {


    @RabbitListener(queues = "topic-queue-male")
    @RabbitHandler
    public void TopicReceiverMale(Map testMessage) {
        log.info("队列：topic-queue-male，消费者收到消息：{}", testMessage);
    }


    @RabbitListener(queues = "topic-queue-female")
    @RabbitHandler
    public void TopicReceiverFemale(Map testMessage) {
        log.info("队列：topic-queue-female，消费者收到消息：{}", testMessage);
    }

    @RabbitListener(queues = "topic-queue-human")
    @RabbitHandler
    public void TopicReceiverHuman(Map testMessage) {
        log.info("队列：topic-queue-human，消费者收到消息：{}", testMessage);
    }


}
