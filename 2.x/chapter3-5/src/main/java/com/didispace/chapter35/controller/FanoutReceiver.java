package com.didispace.chapter35.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: gx.wu
 * @create: 2023-04-23 23:35
 * @Description:
 */

@Slf4j
@Component
public class FanoutReceiver {

    @RabbitListener(queues = "fanout.A")
    @RabbitHandler
    public void FanoutReceiver1(Map testMessage) {
        log.info("fanout.A 消费者收到消息：{}", testMessage);
    }


    @RabbitListener(queues = "fanout.B")
    @RabbitHandler
    public void FanoutReceiver2(Map testMessage) {
        log.info("fanout.B 消费者收到消息：{}", testMessage);
    }


    @RabbitListener(queues = "fanout.C")
    @RabbitHandler
    public void FanoutReceiver3(Map testMessage) {
        log.info("fanout.C 消费者收到消息：{}", testMessage);
    }

}
