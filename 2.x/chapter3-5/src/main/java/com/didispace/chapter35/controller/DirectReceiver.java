package com.didispace.chapter35.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: gx.wu
 * @create: 2023-04-23 23:35
 * @Description:
 */

@Slf4j
@Component
public class DirectReceiver {

    //监听的队列名称 TestDirectQueue
    @RabbitListener(queues = "TestDirectQueue")
    @RabbitHandler
    public void testDirectQueue(Map testMessage) {
        log.info("TestDirectQueue 消费者收到消息：{}", testMessage);
    }


    @RabbitListener(queues = "DemoQueue")
    @RabbitHandler
    public void demoQueue(Map testMessage) {
        log.info("DemoQueue 消费者收到消息：{}", testMessage);
    }

}
