package com.didispace.chapter31;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;

@Data
@NoArgsConstructor
public class User {

    private String name;
    private Integer age;

}